from bs4 import BeautifulSoup
import requests
from dateutil.parser import parse
from datetime import datetime
import csv


def get_all_dates(soup):
    all_dates = []
    print('getting the dates to scrape')
    year_table = soup.find("table", {"class": "tabularData"})
    rows = year_table.findAll("td", {"class": "tableSubHead1"})

    for date_tag in rows:
        current_year = date_tag.findAll('b')
        current_year = current_year[0].text
        all_dates.append(current_year)
        href = date_tag.find_all(href=True)

        for row in href:
            all_dates.append(row.text)

    return all_dates


def scrape(all_dates):
    all_rows = []
    for date in all_dates:
        url = 'https://formerdps.psx.com.pk/webpages/download_internetTrading.php?y='+date
        r = requests.get(url)
        soup = BeautifulSoup(r.text)
        table = soup.find("table", {"class": "tabularData1"})
        all_tr = table.find_all('tr')
        for tr in all_tr:
            date = tr.text
            dt = parse(date)
            date_str = datetime.strftime(dt, '%Y-%m-%d')
            date_str_file_name = datetime.strftime(dt, '%Y-%b-%d')

            year, mon, day = map(str, date_str.split("-"))
            _year, mon_str, _day = map(str, date_str_file_name.split("-"))
            mon_str = mon_str.lower()
            row = ['itsubs', 'pdf', date_str, str(year) + str(day) + str(mon_str) + '.pdf']
            all_rows.append(row)
    with open('itsubs.csv', 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(all_rows)


def run():
    r = requests.get('https://formerdps.psx.com.pk/webpages/download_internetTrading.php?y=2019')
    soup = BeautifulSoup(r.text)
    all_dates = get_all_dates(soup)
    scrape(all_dates)


if __name__ == '__main__':
    run()

