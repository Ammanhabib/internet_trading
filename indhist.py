from bs4 import BeautifulSoup
import requests
from dateutil.parser import parse
from datetime import datetime
import csv


def get_all_dates(soup):
    tables = soup.findAll("table")
    print(tables)
    tables = tables[3]
    dates = tables.getText()
    dates = dates.replace('\n', " ")
    dates = dates.strip()
    dates = dates.split("|")

    return dates


def scrape(all_dates):
    all_rows = []
    for date in all_dates:
        date = date.strip()
        url = 'https://formerweb.psx.com.pk/phps/Constituent_indData.php?y='+date
        r = requests.get(url)
        soup = BeautifulSoup(r.text, 'lxml')
        table = soup.find('table', width="560", cellpadding="3", cellspacing=0)
        all_tr = table.find_all('tr')

        for tr in all_tr:
            if tr.text == '\n':
                continue
            text = tr.text
            row = text.split('\n')
            str_list = list(filter(None, row))
            date = str_list[1]
            dt = parse(date)
            date_str = datetime.strftime(dt, '%Y-%m-%d')
            date_str_file_name = datetime.strftime(dt, '%Y-%b-%d')

            year, mon, day = map(str, date_str.split("-"))
            _year, mon_str, _day = map(str, date_str_file_name.split("-"))
            row = ['indhist', 'xls', date_str, "ind_hstry_" + str(year) + str(day) + str(mon_str) + '.xls']
            all_rows.append(row)

    with open('indhist.csv', 'a') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(all_rows)


def run():
    r = requests.get('https://formerweb.psx.com.pk/phps/Constituent_indData.php?y=2017')
    soup = BeautifulSoup(r.text)
    all_dates = get_all_dates(soup)
    scrape(all_dates)


if __name__ == '__main__':
    run()

